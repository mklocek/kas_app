module Api
  class BaseController < ApplicationController
    protect_from_forgery with: :null_session
    respond_to :json

    before_filter :set_headers
    skip_before_filter :verify_authenticity_token

    private

      def set_headers
        if request.headers["HTTP_ORIGIN"]     
          response.headers['Access-Control-Allow-Origin'] = request.headers['HTTP_ORIGIN']
          response.headers['Access-Control-Expose-Headers'] = 'ETag'
          response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS, HEAD'
          response.headers['Access-Control-Allow-Headers'] = '*,x-requested-with,Content-Type,If-Modified-Since,If-None-Match,Auth-User-Token'
          response.headers['Access-Control-Max-Age'] = '86400'
          response.headers['Access-Control-Allow-Credentials'] = 'true'
        end
      end      

  end
end
