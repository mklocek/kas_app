module Api
  module V1
    class ScoresController < Api::BaseController

      def index
        @scores = Score.all
      end

      def create
        @score = Score.new(score_params)
        if @score.save
          render 'show'
        else
          render json: { message: 'server error' }, status: 500
        end
      end

      private 

        def score_params
          params.require(:score).permit(
                                          :ponts,
                                          :time,
                                          :player_id
                                        )
        end

    end
  end
end