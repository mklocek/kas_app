module Api
  module V1
    class PlayersController < Api::BaseController

      before_action :set_player, only: [:show, :update]

      def index
        # @players = Player.all.sort_by { |p| p.best_score.points }.reverse
        @players = Player.all
      end

      def show
        render json: { message: 'not found' }, status: 404 unless @player
      end

      def create
        @player = Player.new(player_params)
        if @player.save
          render 'show'
        else
          render json: { message: 'server error' }, status: 500
        end
      end

      def update
        if @player.update(player_params)
          render 'show'
        else
          render json: { message: 'server error' }, status: 500
        end
      end

      private 

        def player_params
          params.require(:player).permit(
                                          :nickname,
                                          :age,
                                          :gender,
                                          :phone_uuid
                                        )
        end
    
        def set_player
          @player = Player.find_by_phone_uuid(params[:uuid])
        end

    end
  end
end