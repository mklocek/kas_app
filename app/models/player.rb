class Player < ActiveRecord::Base

  has_many :scores

  public

    def best_score
      self.scores.order('points DESC').first
    end

    def exists? uuid
      true #TODO
    end

end
