json.array! @scores do |score|
  json.points       score.points
  json.time         score.time
  json.player       score.player, 
                      :nickname,
                      :age
end