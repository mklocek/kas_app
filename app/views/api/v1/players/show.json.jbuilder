json.nickname         @player.nickname
json.age              @player.age
json.gender           @player.gender
json.phone_uuid       @player.phone_uuid
json.best_score       @player.best_score
json.scores           @player.scores,
                        :points,
                        :time