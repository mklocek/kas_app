require 'faker'

FactoryGirl.define do
  factory :player do
    nickname                    Faker::Name.first_name
    age                         Faker::Number.number(2)
    gender                      false
    phone_uuid                  Faker::Lorem.word
  end
end