require 'spec_helper'

describe "Players API" do
  describe "GET /players" do
    it "returns all the players" do
      player1 = create(:player, nickname: "Franek")
      player2 = create(:player, nickname: "Jadzia")

      get "/api/v1/players", {}, { "Accept" => "application/json" }

      expect(response.status).to eq 200

      body = JSON.parse(response.body)
      players = body.map { |m| m["nickname"] }

      expect(players).to match_array(["Franek", "Jadzia"])
    end
  end
end