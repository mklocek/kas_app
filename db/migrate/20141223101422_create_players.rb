class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :nickname
      t.integer :age
      t.boolean :gender
      t.string :phone_uuid

      t.timestamps
    end
  end
end
