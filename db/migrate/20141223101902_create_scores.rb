class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.integer :points
      t.integer :time
      t.integer :player_id

      t.timestamps
    end
  end
end
