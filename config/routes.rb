Rails.application.routes.draw do
  
  root 'main#index'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do 

      scope '/players' do
        get '/' => 'players#index'
        post '/create' => 'players#create'
        scope '/:uuid' do
          get '/' => 'players#show'
          put '/' => 'players#update'
        end
      end

      scope '/scores' do 
        get '/' => 'scores#index'
        post '/' => 'scores#create'
      end

    end
  end

end
